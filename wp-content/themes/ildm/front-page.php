<?php
get_header();
?>
<!--========================================================
                          CONTENT
=========================================================-->
<main>
    <section class="well well1 well1_ins1">
        <?php
        $slider_args = array(
            'post_type' => 'slider',
            'posts_per_page' => -1,
        );
        $slider_posts = new WP_Query($slider_args);
        if ($slider_posts->have_posts()):
            $i = 0;
            ?>
            <div class="camera_container">
                <div id="camera" class="camera_wrap">
                    <?php while ($slider_posts->have_posts()): ?>
                        <?php
                        $slider_posts->the_post();
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                        $link = CFS()->get('link');
                        $content = CFS()->get('content');
                        ?>
                        <div data-src="<?php echo $image_url[0]; ?>">
                            <div class="camera_caption fadeIn">
                                <div class="jumbotron jumbotron1">
                                    <em>
                                        <?php the_title(); ?>
                                    </em>
                                    <div class="wrap">
                                        <?php if (!empty($content)): ?>
                                            <p>
                                                <?php echo $content; ?>
                                            </p>
                                        <?php endif; ?>
                                        <?php if (!empty($link)): ?>
                                            <a href="<?php echo $link; ?>" class="btn-link fa-angle-right"></a>
                                        <?php endif; ?>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <?php $i++; ?>
                    <?php endwhile; ?>
                </div>
            </div>

        <?php endif; ?>
        <?php $curr_lang = pll_current_language(); ?>
        <?php $settings = get_option("hma_theme_settings"); ?>
        <div class="container center991">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id1 = $settings['hma_home_block1'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id1 = $settings['hma_home_block1_ml'];
                        }
                        $block_id1 = empty($ml_block_id1) ? $en_block_id1 : $ml_block_id1;
                        query_posts("p=$block_id1&post_type=page");
                        if (have_posts()):
                            $i = 0;
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img1.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 75) {
                                            $pos = strpos($content, ' ', 75);
                                            echo substr($content, 0, $pos);
                                        } else {
                                            echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>              
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id2 = $settings['hma_home_block2'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id2 = $settings['hma_home_block2_ml'];
                        }
                        $block_id2 = empty($ml_block_id2) ? $en_block_id2 : $ml_block_id2;
                        query_posts("p=$block_id2&post_type=page");
                        if (have_posts()):
                            $i = 0;
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img2.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg3">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p class="thumb_ins1">
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 75) {
                                            $pos = strpos($content, ' ', 75);
                                            echo substr($content, 0, $pos);
                                        } else {
                                            echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div> 
                </div>
            </div>

            <div class="row wow fadeIn" data-wow-duration='2s'>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id3 = $settings['hma_home_block3'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id3 = $settings['hma_home_block3_ml'];
                        }
                        $block_id3 = empty($ml_block_id3) ? $en_block_id3 : $ml_block_id3;
                        query_posts("p=$block_id3&post_type=page");
                        if (have_posts()):
//                            $i = 0;
//                            
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img3.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg3">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                            echo substr($content, 0, $pos);
                                        } else {
                                            echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>              
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id4 = $settings['hma_home_block4'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id4 = $settings['hma_home_block4_ml'];
                        }
                        $block_id4 = empty($ml_block_id4) ? $en_block_id4 : $ml_block_id4;
                        query_posts("p=$block_id4&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img4.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg-primary">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                            echo substr($content, 0, $pos);
                                        } else {
                                            echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>              
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id5 = $settings['hma_home_block5'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id5 = $settings['hma_home_block5_ml'];
                        }
                        $block_id5 = empty($ml_block_id5) ? $en_block_id5 : $ml_block_id5;
                        query_posts("p=$block_id5&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            // $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           // echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

            </div>
            <div class="row wow fadeIn" data-wow-duration='2s'>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id6 = $settings['hma_home_block6'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id6 = $settings['hma_home_block6_ml'];
                        }
                        $block_id6 = empty($ml_block_id6) ? $en_block_id6 : $ml_block_id6;
                        query_posts("p=$block_id6&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id7 = $settings['hma_home_block7'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id7 = $settings['hma_home_block7_ml'];
                        }
                        $block_id7 = empty($ml_block_id7) ? $en_block_id7 : $ml_block_id7;
                        query_posts("p=$block_id7&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id8 = $settings['hma_home_block8'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id8 = $settings['hma_home_block8_ml'];
                        }
                        $block_id8 = empty($ml_block_id8) ? $en_block_id8 : $ml_block_id8;
                        query_posts("p=$block_id8&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>

            <div class="row wow fadeIn" data-wow-duration='2s'>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id9 = $settings['hma_home_block9'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id9 = $settings['hma_home_block9_ml'];
                        }
                        $block_id9 = empty($ml_block_id9) ? $en_block_id9 : $ml_block_id9;
                        query_posts("p=$block_id9&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id10 = $settings['hma_home_block10'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id10 = $settings['hma_home_block10_ml'];
                        }
                        $block_id10 = empty($ml_block_id10) ? $en_block_id10 : $ml_block_id10;
                        query_posts("p=$block_id10&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id11 = $settings['hma_home_block11'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id11 = $settings['hma_home_block11_ml'];
                        }
                        $block_id11 = empty($ml_block_id11) ? $en_block_id11 : $ml_block_id11;
                        query_posts("p=$block_id11&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
            <div class="row wow fadeIn" data-wow-duration='2s'>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id12 = $settings['hma_home_block12'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id12 = $settings['hma_home_block12_ml'];
                        }
                        $block_id12 = empty($ml_block_id12) ? $en_block_id12 : $ml_block_id12;
                        query_posts("p=$block_id12&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id13 = $settings['hma_home_block13'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id13 = $settings['hma_home_block13_ml'];
                        }
                        $block_id13 = empty($ml_block_id13) ? $en_block_id13 : $ml_block_id13;
                        query_posts("p=$block_id13&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id14 = $settings['hma_home_block14'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id14 = $settings['hma_home_block14_ml'];
                        }
                        $block_id14 = empty($ml_block_id14) ? $en_block_id14 : $ml_block_id14;
                        query_posts("p=$block_id14&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
            <div class="row wow fadeIn" data-wow-duration='2s'>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id15 = $settings['hma_home_block15'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id15 = $settings['hma_home_block15_ml'];
                        }
                        $block_id15 = empty($ml_block_id15) ? $en_block_id15 : $ml_block_id15;
                        query_posts("p=$block_id15&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id16 = $settings['hma_home_block16'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id16 = $settings['hma_home_block16_ml'];
                        }
                        $block_id16 = empty($ml_block_id16) ? $en_block_id16 : $ml_block_id16;
                        query_posts("p=$block_id16&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail thumb-shadow">
                        <?php
                        $en_block_id16 = $settings['hma_home_block16'];
                        if ($curr_lang == 'ml') {
                            $ml_block_id5 = $settings['hma_home_block16_ml'];
                        }
                        $block_id16 = empty($ml_block_id16) ? $en_block_id16 : $ml_block_id16;
                        query_posts("p=$block_id16&post_type=page");
                        if (have_posts()):
                            ?>
                            <?php
                            the_post();
                            $img_id = CFS()->get('home_page_image');
                            $image_url = wp_get_attachment_image_src($img_id, 'full');
                            if (!empty($image_url)) {
                                ?>
                                <img src="<?php echo $image_url[0]; ?>" alt="">
                            <?php } else { ?>
                                <img src="<?php bloginfo('template_url') ?>/images/page-1_img5.jpg" alt="">
                            <?php } ?>
                            <div class="caption bg2">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <div class="wrap">
                                    <p>
                                        <?php
                                        $string = strip_tags(get_the_content());
                                        $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                        if (strlen($content) > 45) {
                                            $pos = strpos($content, ' ', 45);
                                           echo substr($content, 0, $pos);
                                        } else {
                                           echo $content;
                                        }
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn-link fa-angle-right"></a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>

            </div>
        </div>        
    </section>
    <?php wp_reset_query(); ?>
    <section class="well well2 wow fadeIn  bg1" data-wow-duration='3s'>
        <div class="container">
            <?php
            query_posts('p=36&post_type=page');
            if (have_posts()):
                $i = 0;
                ?>
                <?php
                the_post();
                $first_p = CFS()->get('first_paragraph');
                $sec_p = CFS()->get('second_paragraph');
                ?>
                <h3 class="txt-pr">
                    <?php the_title(); ?>
                </h3>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>
                            <?php echo $first_p; ?>
                        </p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p>
                            <?php echo $sec_p; ?>
                            <a href="<?php the_permalink(); ?>" class="btn-link l-h1 fa-angle-right"></a>
                        </p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <section class="well well2">
        <div class="container">
            <!--            <h2>
                            our
                            <small>
                                Trainings
                            </small>
                        </h2>-->
            <div class="row offs1">
                <div class="col-md-6 col-sm-12">
                    <?php
//                    $training_args = array(
//                        'post_type' => 'training',
//                        'posts_per_page' => -1,
//                    );
//                    $training_posts = new WP_Query($training_args);
//                    if ($training_posts->have_posts()):
//                        $i = 0;
                    ?>
                    <!--<ul class="link-list wow fadeInLeft" data-wow-duration='3s'>-->
                    <?php // while ($training_posts->have_posts()): ?>
                    <?php
//                                $training_posts->the_post();
//                                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
//                                $link = CFS()->get('link');
//                                $content = CFS()->get('content');
                    ?>
                    <!--                                <li>
                                                        <a href="<?php echo $link; ?>"><?php the_title(); ?></a>
                    
                                                        <a href="<?php echo $link; ?>" class="btn-link l-h1 fa-angle-right"></a>
                                                    </li>-->
                    <?php // $i++; ?>
                    <?php // endwhile; ?>
                    <!--</ul>-->
                    <?php // endif; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-list-alt"></span><b>News</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php
                                    $news_args = array(
                                        'post_type' => 'news',
                                        'posts_per_page' => -1,
                                    );
                                    $news_posts = new WP_Query($news_args);
                                    if ($news_posts->have_posts()):
                                        $i = 0;
                                        ?>
                                        <ul class="demo1">
                                            <?php while ($news_posts->have_posts()): ?>
                                                <?php
                                                $news_posts->the_post();
                                                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                                                $link = CFS()->get('link');
                                                $content = CFS()->get('content');
                                                ?>
                                                <li class="news-item">
                                                    <h4><?php the_title(); ?></h4>
                                                    <?php
                                                    $string = strip_tags(get_the_content());
                                                    $content = preg_replace('/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);
                                                    if (strlen($content) > 50) {
                                                        $pos = strpos($content, ' ', 50);
                                                        echo substr($content, 0, $pos);
                                                    } else {
                                                        echo $content;
                                                    }
                                                    ?> <a href="<?php the_permalink(); ?>">Read more...</a>

                                                </li>
                                                <?php $i++; ?>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <!--<img class="width_img" src="<?php bloginfo('template_url') ?>/images/page-1_img6.jpg" alt="">-->
                    <!--<img class="width_img" src="http://localhost/ildmkerala/wp-content/uploads/2016/08/570-2.jpg" alt="">-->
                    <?php dynamic_sidebar('upcoming_events_widget_area'); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="well well3 parallax" data-url="<?php bloginfo('template_url') ?>/images/parallax1.jpg" data-mobile="
             true" data-speed="0.9">
        <div class="container">
            <div class="wrap text-center">
                <?php
                query_posts('p=77&post_type=page');
                if (have_posts()):
                    ?>
                    <?php
                    the_post();
                    ?>
                    <strong>
                        <!--                    SAVE TIME,<br />
                                            SAVE MONEY,
                                            <small>
                                                GROW & SUCCEED
                                            </small>-->
                        <?php the_title(); ?>
                    </strong>
                    <p>
                        <?php
                        $content = strip_tags(get_the_content());
                        if (strlen($content) > 1000) {
                            $pos = strpos($content, ' ', 1000);
                            echo substr($content, 0, $pos);
                        } else {
                            echo $content;
                        }
                        ?>
                    </p>
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary">read more <span class="fa-angle-right"></span></a>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>  
        </div>        
    </section>
</main>
<?php
get_footer();
?>
<script src="<?php bloginfo('template_url') ?>/js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $(".demo1").bootstrapNews({
            newsPerPage: 5,
            autoplay: true,
//            pauseOnHover: true,
            direction: 'up',
            newsTickerInterval: 4000
        });
    });
</script>
<style>
    .demo1 .glyphicon
    {
        margin-right:4px !important; /*override*/
    }

    .demo1 .pagination .glyphicon
    {
        margin-right:0px !important; /*override*/
    }

    .demo1 .pagination a
    {
        color:#555;
    }

    .demo1 .panel ul
    {
        padding:0px;
        margin:0px;
        list-style:none;
    }

    .demo1 .news-item
    {
        padding:4px 4px;
        margin:0px;
        border-bottom:1px dotted #555; 
    }
</style>
<?php

/*
 * Theme Functions
 */
define('AC_THEME_IMAGES', get_template_directory() . '/images/');

//Support for menus
add_theme_support('menus');
//Support for Featured images
add_theme_support('post-thumbnails');

// disable admin bar from fron-end
show_admin_bar(false);

if (!function_exists('ac_theme_setup')):

    function ac_theme_setup() {
        /*   backend option for managing Main menu.   */
        register_nav_menus(array(
            'main-menu' => __('Main Menu', 'ac'),
            'footer-menu' => __('Footer Menu', 'ac'),
        ));
    }

endif;
// Functions executed in theme init
add_action('after_setup_theme', 'ac_theme_setup');

require_once( get_template_directory() . '/inc/custom-post-types.php' );

require_once( get_template_directory() . '/inc/settings.php' );
require_once( get_template_directory() . '/inc/participants_list.php' );
require_once( get_template_directory() . '/inc/faculty_list.php' );
require_once( get_template_directory() . '/inc/facility_list.php' );

function ac_scripts_with_jquery() {
    wp_register_script('bs-script', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_register_script('nice-scroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'));
    wp_register_script('my-script', get_template_directory_uri() . '/js/script.js');
    wp_enqueue_script('bs-script');
    wp_enqueue_script('nice-scroll');
    wp_enqueue_script('my-script');
}

//add_action('wp_enqueue_scripts', 'ac_scripts_with_jquery');
// Widget support functions starts
add_action('widgets_init', 'hma_widgets_init');

function hma_widgets_init() {
    register_sidebar(array(
        'name' => 'Upcoming events widget area',
        'id' => 'upcoming_events_widget_area',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="up-events-widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Language widget area',
        'id' => 'language_widget_area',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="language-widget-title">',
        'after_title' => '</h2>',
    ));
}

function new_excerpt_more($more) {
    return '...';
}

add_filter('excerpt_more', 'new_excerpt_more');

function change_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/', '/ class="dropdown-menu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'change_submenu_class');

class Selective_Walker extends Walker_Nav_Menu {

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        $id_field = $this->db_fields['id'];

        if (is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    function start_el(&$output, $item, $depth, $args) {
        if ($args->has_children) {
            $item->classes[] = 'dropdown';
        }

        parent::start_el($output, $item, $depth, $args);
    }

}

<?php
/*
  Template Name: Participant Registration
 */
?>
<?php
get_header();
?>
<?php
global $wpdb;
if (isset($_POST['submit']) && $_POST['submit'] == "Submit") {
    $data['training_course_id'] = $training_name = sanitize_text_field($_POST['training_name']);
    $training_date = sanitize_text_field($_POST['training_date']);
    if (!empty($training_date)) {
        list($d, $m, $y) = explode('/', $training_date);
        $data['training_date'] = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
    } else {
        $data['training_date'] = '';
    }
    $data['trainee_name'] = $trainee_name = sanitize_text_field($_POST['trainee_name']);
    $data['gender'] = $gender = sanitize_text_field($_POST['gender']);
    $dob = sanitize_text_field($_POST['dob']);
    if (!empty($dob)) {
        list($d, $m, $y) = explode('/', $dob);
        $data['dob'] = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
    } else {
        $data['dob'] = '';
    }
    $data['designation'] = $designation = sanitize_text_field($_POST['designation']);
    $data['seniority_num'] = $seniority_num = sanitize_text_field($_POST['seniority_num']);
    $data['office'] = $office = sanitize_text_field($_POST['office']);
    $data['office_taluk'] = $office_taluk = sanitize_text_field($_POST['office_taluk']);
    $data['office_district'] = $office_district = sanitize_text_field($_POST['office_district']);
    $data['office_phone'] = $office_phone = sanitize_text_field($_POST['office_phone']);
    $data['pen_num'] = $pen_num = sanitize_text_field($_POST['pen_num']);
    $data['auth_officer'] = $auth_officer = sanitize_text_field($_POST['auth_officer']);
    $data['proced_num'] = $proced_num = sanitize_text_field($_POST['proced_num']);

    $data['address'] = $address = sanitize_text_field($_POST['address']);
    $data['home_district'] = $home_district = sanitize_text_field($_POST['home_district']);
    $data['home_pincode'] = $home_pincode = sanitize_text_field($_POST['home_pincode']);
    $data['mobile_num'] = $mobile_num = sanitize_text_field($_POST['mobile_num']);

    $data['edu_qlfn'] = $edu_qlfn = sanitize_text_field($_POST['edu_qlfn']);
    $data['tech_know'] = $tech_know = sanitize_text_field($_POST['tech_know']);
    $data['train_attend'] = $train_attend = sanitize_text_field($_POST['train_attend']);
    $data['room_fac'] = $room_fac = sanitize_text_field($_POST['room_fac']);

    $query = $wpdb->insert($wpdb->prefix . "participants", $data);
}
?>
<main>
    <section class="well well4">
        <div class="container">
            <h2>
                <?php the_title(); ?>
            </h2>
            <?php if (isset($query)): ?>
                <div class="alert alert-success">Registered successfully</div>
            <?php endif; ?>
            <div class="row offs2">
                <div class="col-lg-12">
                    <form method="post" action="" class="commonform">
                        <div class="form-group">
                            <label for="training_name">Select Training</label>
                            <select class="form-control required" name="training_name">
                                <option>--Select--</option>
                                <?php
                                $course_args = array(
                                    'post_type' => 'course',
                                    'posts_per_page' => -1,
                                );
                                $course_posts = new WP_Query($course_args);
                                if ($course_posts->have_posts()):
                                    ?>
                                    <?php while ($course_posts->have_posts()): ?>
                                        <?php $course_posts->the_post(); ?>
                                        <option value="<?php the_ID(); ?>"><?php the_title(); ?></option>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="training_date">Training Date</label>
                            <input type="input" class="form-control required" name="training_date" id="training_date" placeholder="Training Date">
                        </div>
                        <fieldset>
                            <legend>Personal Details</legend>
                            <div class="form-group">
                                <label for="trainee_name">Name</label>
                                <input type="input" class="form-control required" id="trainee_name" name="trainee_name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="form-control" id="gender" name="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="dob">Date of Birth</label>
                                <input type="input" class="form-control required" name="dob" id="dob" placeholder="Date of Birth">
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Office Details</legend>
                            <div class="form-group">
                                <label for="designation">Designation</label>
                                <input type="input" class="form-control required" name="designation" id="designation" placeholder="Designation">
                            </div>
                            <div class="form-group">
                                <label for="seniority_num">Seniority Number</label>
                                <input type="input" class="form-control required" name="seniority_num" id="seniority_num" placeholder="Seniority Number">
                            </div>
                            <div class="form-group">
                                <label for="office">Office</label>
                                <input type="input" class="form-control required" name="office" id="office" placeholder="Office">
                            </div>
                            <div class="form-group">
                                <label for="office_taluk">Taluk</label>
                                <input type="input" class="form-control required" name="office_taluk" id="office_taluk" placeholder="Taluk">
                            </div>
                            <div class="form-group">
                                <label for="office_district">District</label>
                                <input type="input" class="form-control required" name="office_district" id="office_district" placeholder="District">
                            </div>
                            <div class="form-group">
                                <label for="office_phone">Phone (Office)</label>
                                <input type="input" class="form-control required" name="office_phone" id="office_phone" placeholder="Phone Number">
                            </div>
                            <div class="form-group">
                                <label for="pen_num">Pen Number</label>
                                <input type="input" class="form-control required" name="pen_num" id="pen_num" placeholder="Pen Number">
                            </div>
                            <div class="form-group">
                                <label for="auth_officer">Authorised Officer</label>
                                <input type="input" class="form-control required" name="auth_officer" id="auth_officer" placeholder="Authorised Officer">
                            </div>
                            <div class="form-group">
                                <label for="proced_num">Procedure Number</label>
                                <input type="input" class="form-control required" name="proced_num" id="proced_num" placeholder="Procedure Number">
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Resident Details</legend>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="input" class="form-control required" name="address" id="address" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <label for="home_district">District</label>
                                <input type="input" class="form-control required" name="home_district" id="home_district" placeholder="District">
                            </div>
                            <div class="form-group">
                                <label for="home_pincode">Pincode</label>
                                <input type="input" class="form-control required number" name="home_pincode" id="home_pincode" placeholder="Pincode">
                            </div>
                            <div class="form-group">
                                <label for="mobile_num">Mobile Number</label>
                                <input type="input" class="form-control required" name="mobile_num" id="mobile_num" placeholder="Mobile Number">
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Additional Details</legend>
                            <div class="form-group">
                                <label for="edu_qlfn">Educational Qualification</label>
                                <input type="input" class="required form-control" name="edu_qlfn" id="edu_qlfn" placeholder="Educational Qualification">
                            </div>
                            <div class="form-group">
                                <label for="tech_know">Technical Knowledge</label>
                                <input type="input" class="required form-control" name="tech_know" id="tech_know" placeholder="Technical Knowledge">
                            </div>
                            <div class="form-group">
                                <label for="Name">Have you attended any training section in this center before? </label>
                                <input type="radio" name="train_attend" id="train_attend1" class="required" value="1"> Yes
                                <input type="radio" name="train_attend" id="train_attend2" class="required" value="0"> No
                            </div>

                            <div class="form-group">
                                <label for="Name">Hostel Facility required? </label>
                                <input type="radio" name="room_fac" id="room_fac1" class="required" value="1"> Yes
                                <input type="radio" name="room_fac" id="room_fac2" class="required" value="0"> No
                            </div>
                        </fieldset>
                        <input type="submit" name="submit" value="Submit" class="btn">
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
?>
<link href="<?php bloginfo('template_url') ?>/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="<?php bloginfo('template_url') ?>/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/validate.css">
<script src="<?php bloginfo('template_url') ?>/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".commonform").validate();
        jQuery('#training_date, #dob').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    });
</script>
<?php
get_header();
?>
<main>
    <section class="well well4">
        <div class="container">
            <h2>
                404 - Page Not Found
            </h2>
            <div class="row offs2">
                <div class="col-lg-12">
                    <p class="not-pera">Looks Like Something Went Wrong!</p>
                    <p class="not-sub">The page you were looking for is not here.</p>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
get_footer();

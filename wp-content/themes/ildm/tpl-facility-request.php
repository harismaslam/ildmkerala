<?php
/*
  Template Name: Facility Request
 */
?>
<?php
get_header();
?>
<?php
global $wpdb;
if (isset($_POST['submit']) && $_POST['submit'] == "Submit") {
    $data['appcnt_name'] = $appcnt_name = sanitize_text_field($_POST['appcnt_name']);
    $data['appcnt_addr'] = $appcnt_addr = sanitize_text_field($_POST['appcnt_addr']);
    $data['appcnt_addr_priv'] = $appcnt_addr_priv = sanitize_text_field($_POST['appcnt_addr_priv']);
    $data['appcnt_cnt_num'] = $appcnt_cnt_num = sanitize_text_field($_POST['appcnt_cnt_num']);
    $data['appcnt_email'] = $appcnt_email = sanitize_text_field($_POST['appcnt_email']);


    $data['walkway'] = $walkway = isset($_POST['walkway']) ? 1 : 0;
    $data['classroom'] = $classroom = isset($_POST['classroom']) ? 1 : 0;
    $data['smart_classroom'] = $smart_classroom = isset($_POST['smart_classroom']) ? 1 : 0;
    $data['seminar_hall'] = $seminar_hall = isset($_POST['seminar_hall']) ? 1 : 0;
    $data['room'] = $room = isset($_POST['room']) ? 1 : 0;
    $data['exec_room'] = $exec_room = isset($_POST['exec_room']) ? 1 : 0;

    $data['num_days'] = $num_days = sanitize_text_field($_POST['num_days']);

    $query = $wpdb->insert($wpdb->prefix . "facilities", $data);
}
?>
<main>
    <section class="well well4">
        <div class="container">
            <h2>
                <?php the_title(); ?>
            </h2>
            <?php if (isset($query)): ?>
                <div class="alert alert-success">Registered successfully</div>
            <?php endif; ?>
            <div class="row offs2">
                <div class="col-lg-12">
                    <form method="post" action="" class="commonform">
                        <div class="form-group">
                            <label for="appcnt_name">Name of Applicant </label>
                            <input type="input" class="form-control required" name="appcnt_name" id="appcnt_name" placeholder="Applicant Name">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_addr">Address of Applicant (Official)</label>
                            <input type="input" class="form-control required" name="appcnt_addr" id="appcnt_addr" placeholder="Address of Applicant">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_addr_priv">Address of Applicant (Private: Non Officials)</label>
                            <input type="input" class="form-control required" name="appcnt_addr_priv" id="appcnt_addr_priv" placeholder="Address of Applicant (Private)">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_cnt_num">Contact Number</label>
                            <input type="input" class="form-control required" name="appcnt_cnt_num" id="appcnt_cnt_num" placeholder="Contact Number">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_email">Email Id</label>
                            <input type="input" class="form-control required" name="appcnt_email" id="appcnt_email" placeholder="Email Id">
                        </div>
                        <div class="form-group">
                            <label for="">Facility Required</label>
                            <input type="checkbox" value="1" class="" name="walkway" id="walkway"> Walkway
                            <input type="checkbox" value="1" class="" name="classroom" id="classroom"> Classroom
                            <input type="checkbox" value="1" class="" name="smart_classroom" id="smart_classroom"> Smart Classroom
                            <input type="checkbox" value="1" class="" name="seminar_hall" id="seminar_hall"> Seminar Hall
                            <input type="checkbox" value="1" class="" name="room" id="room"> Room
                            <input type="checkbox" value="1" class="" name="exec_room" id="exec_room"> Executive Room
                        </div>
                        <div class="form-group">
                            <label for="num_days">Number of days Required</label>
                            <input type="input" class="form-control" name="num_days" id="num_days" placeholder="Number of days">
                        </div>
                        <input type="submit" name="submit" value="Submit" class="btn">
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
get_footer();
?>
<link href="<?php bloginfo('template_url') ?>/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="<?php bloginfo('template_url') ?>/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/validate.css">
<script src="<?php bloginfo('template_url') ?>/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".commonform").validate();
        jQuery('#training_date, #dob').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    });
</script>

<?php
get_header();
?>
<main>        

    <section class="well well4">
        <div class="container">
            <h2>
                <?php the_title(); ?>
            </h2>
            <div class="row offs2">
                <div class="col-lg-12">
                    <?php
                    if (have_posts()):
                        the_post();
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                        $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
                        ?>
                        <?php if (!empty($image_url)): ?>
                            <img src="<?php echo $image_url[0]; ?>" alt="<?php echo!empty($alt) ? $alt : 'gnwp'; ?>">
                        <?php endif; ?>
                        <?php the_content(); ?>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
get_footer();
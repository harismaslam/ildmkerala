<?php
/*
  Template Name: Faculty Registration
 */
?>
<?php
get_header();
?>
<?php
global $wpdb;
if (isset($_POST['submit']) && $_POST['submit'] == "Submit") {
    $data['appcnt_name'] = $appcnt_name = sanitize_text_field($_POST['appcnt_name']);
    $data['appcnt_addr'] = $appcnt_addr = sanitize_text_field($_POST['appcnt_addr']);
    $data['appcnt_cnt_num'] = $appcnt_cnt_num = sanitize_text_field($_POST['appcnt_cnt_num']);
    $data['appcnt_email'] = $appcnt_email = sanitize_text_field($_POST['appcnt_email']);
    
    $data['edu_doct'] = $edu_doct = sanitize_text_field($_POST['edu_doct']);
    $data['edu_pg'] = $edu_pg = sanitize_text_field($_POST['edu_pg']);
    $data['edu_pg_dip'] = $edu_pg_dip = sanitize_text_field($_POST['edu_pg_dip']);
    $data['edu_dg'] = $edu_dg = sanitize_text_field($_POST['edu_dg']);
    $data['edu_dip'] = $edu_dip = sanitize_text_field($_POST['edu_dip']);
    $data['edu_cert'] = $edu_cert = sanitize_text_field($_POST['edu_cert']);
    
    $data['exp_gov'] = $exp_gov = sanitize_text_field($_POST['exp_gov']);
    $data['exp_prv'] = $exp_prv = sanitize_text_field($_POST['exp_prv']);
    $data['teach_exp'] = $teach_exp = sanitize_text_field($_POST['teach_exp']);
    $data['sub_prof'] = $sub_prof = sanitize_text_field($_POST['sub_prof']);
    
//    $training_date = sanitize_text_field($_POST['training_date']);
//    if (!empty($training_date)) {
//        list($d, $m, $y) = explode('/', $training_date);
//        $data['training_date'] = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
//    } else {
//        $data['training_date'] = '';
//    }
    
    $data['lang_ml'] = $lang_ml = isset($_POST['lang_ml']) ? 1 : 0;
    $data['lang_en'] = $lang_en = isset($_POST['lang_en']) ? 1 : 0;
    $data['lang_hi'] = $lang_hi = isset($_POST['lang_hi']) ? 1 : 0;
    $data['lang_tn'] = $lang_tn = isset($_POST['lang_tn']) ? 1 : 0;
    $data['lang_ka'] = $lang_ka = isset($_POST['lang_ka']) ? 1 : 0;
    
    $query = $wpdb->insert($wpdb->prefix . "faculties", $data);
}
?>
<main>
    <section class="well well4">
        <div class="container">
            <h2>
                <?php the_title(); ?>
            </h2>
            <?php if (isset($query)): ?>
                <div class="alert alert-success">Registered successfully</div>
            <?php endif; ?>
            <div class="row offs2">
                <div class="col-lg-12">
                    <form method="post" action="" class="commonform">
                        <div class="form-group">
                            <label for="appcnt_name">Applicant Name</label>
                            <input type="input" class="form-control required" name="appcnt_name" id="appcnt_name" placeholder="Applicant Name">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_addr">Address of Applicant</label>
                            <input type="input" class="form-control required" name="appcnt_addr" id="appcnt_addr" placeholder="Address of Applicant">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_cnt_num">Contact Number</label>
                            <input type="input" class="form-control required" name="appcnt_cnt_num" id="appcnt_cnt_num" placeholder="Contact Number">
                        </div>
                        <div class="form-group">
                            <label for="appcnt_email">Email Id</label>
                            <input type="input" class="form-control required" name="appcnt_email" id="appcnt_email" placeholder="Email Id">
                        </div>
                        <fieldset>
                            <legend>Educational Qualifications</legend>
                            <div class="form-group">
                                <label for="edu_doct">Doctorate</label>
                                <input type="input" class="form-control" name="edu_doct" id="edu_doct" placeholder="Doctorate">
                            </div>
                            <div class="form-group">
                                <label for="edu_pg">Post Graduation</label>
                                <input type="input" class="form-control" name="edu_pg" id="edu_pg" placeholder="Post Graduation">
                            </div>
                            <div class="form-group">
                                <label for="edu_pg_dip">PG Diploma</label>
                                <input type="input" class="form-control" name="edu_pg_dip" id="edu_pg_dip" placeholder="PG Diploma">
                            </div>
                            <div class="form-group">
                                <label for="edu_dg">Graduation</label>
                                <input type="input" class="form-control" name="edu_dg" id="edu_dg" placeholder="Graduation">
                            </div>
                            <div class="form-group">
                                <label for="edu_dip">Diploma</label>
                                <input type="input" class="form-control" name="edu_dip" id="edu_dip" placeholder="Diploma">
                            </div>
                            <div class="form-group">
                                <label for="edu_cert">Certificate</label>
                                <input type="input" class="form-control" name="edu_cert" id="edu_cert" placeholder="Certificate">
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Job experience</legend>
                            <div class="form-group">
                                <label for="exp_gov">Government</label>
                                <input type="input" class="form-control" name="exp_gov" id="exp_gov" placeholder="Government">
                            </div>
                            <div class="form-group">
                                <label for="exp_prv">Private</label>
                                <input type="input" class="form-control" name="exp_prv" id="exp_prv" placeholder="Private">
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="teach_exp">Teaching Experience (in years)</label>
                            <input type="input" class="form-control" name="teach_exp" id="teach_exp" placeholder="Teaching Experience">
                        </div>
                        <div class="form-group">
                            <label for="sub_prof">Subjects of proficiency</label>
                            <input type="input" class="form-control" name="sub_prof" id="sub_prof" placeholder="Subjects of proficiency">
                        </div>
                        <div class="form-group">
                            <label for="">Language proficiency for dealing the class</label>
                            <input type="checkbox" value="1" class="" name="lang_ml" id="sub_prof"> Malayalam
                            <input type="checkbox" value="1" class="" name="lang_en" id="sub_prof"> English
                            <input type="checkbox" value="1" class="" name="lang_hi" id="sub_prof"> Hindi
                            <input type="checkbox" value="1" class="" name="lang_tn" id="sub_prof"> Tamil
                            <input type="checkbox" value="1" class="" name="lang_ka" id="sub_prof"> Kannada
                        </div>
                        <input type="submit" name="submit" value="Submit" class="btn">
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
get_footer();
?>
<link href="<?php bloginfo('template_url') ?>/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="<?php bloginfo('template_url') ?>/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/validate.css">
<script src="<?php bloginfo('template_url') ?>/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".commonform").validate();
        jQuery('#training_date, #dob').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    });
</script>

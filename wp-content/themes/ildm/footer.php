<!--========================================================
                                    FOOTER
          =========================================================-->
<footer class="top-border">
<?php wp_reset_query(); ?>
    <?php if (is_front_page() || get_the_ID()==216): ?>
        <div class="map">
            <div id="google-map" class="map_model" data-zoom="11" data-x="76.9883183" data-y="8.5115883"></div>
            <ul class="map_locations">
                <li data-x="76.9883183" data-y="8.5115883" data-basic="<?php bloginfo('template_url') ?>/images/gmap_marker.png" data-active="images/gmap_marker_active.png">
                    <div class="location">
                        <h3 class="txt-clr1">
                            Institute of Land and Disaster Management
    <!--                        <small>
                                COMPANY
                            </small>-->
                        </h3>  
                        <address>
                            <dl>
                                <dt>Phone Number: </dt>
                                <dd class="phone"><a href="callto:+914712365559">0471 - 2365559</a></dd>
                            </dl>
                            <dl>
                                <dt>Address: </dt>
                                <dd>PTP Nagar,  Thiruvananthapuram - 38</dd>
                            </dl>
                            <dl>
                                <dt>Hours: </dt>
                                <dd> 10am - 5pm</dd>
                            </dl>
                            <dl>
                                <dt> E-mail: </dt>
                                <dd><a href="mailto:ilmsecretary@yahoo.com">ilmsecretary[a]yahoo.com</a></dd>
                            </dl>
                        </address>

                    </div>
                </li>
            </ul>
        </div>
    <?php endif; ?>

    <section class="well1">
        <div class="container">
            <div class="row">
                <!--<div class="col-sm-6 col-xs-12">
                    <p class="rights">
                        ILDM  &#169; <span id="copyright-year"></span>
                    </p>
                </div>-->
                <div class="col-sm-12 col-xs-12">
                    <?php wp_nav_menu(array('container' => false, 'menu_class' => 'list-inline', 'theme_location' => 'footer-menu')); ?>
                </div>
            </div>
        </div> 
    </section>    
</footer>
</div>
<?php wp_footer(); ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->         
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>

<!--cookie.JS-->
<script src="<?php bloginfo('template_url') ?>/js/jquery.cookie.js"></script>
<!--Easing library-->
<script src="<?php bloginfo('template_url') ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/tmstickup.js"></script>


<!--ToTop-->
<script src="<?php bloginfo('template_url') ?>/js/jquery.ui.totop.js"></script>
<!--EqualHeights-->
<script>
    var o = jQuery('[data-equal-group]');
    if (o.length > 0) {
        var alertJs = document.createElement('script');
        alertJs.type = 'text/javascript';
        alertJs.src = '<?php bloginfo('template_url') ?>/js/jquery.equalheights.js';
        jQuery('body').append(alertJs);
    }
    function isIE() {
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
    }
</script>
<script src="<?php bloginfo('template_url') ?>/js/superfish.js"></script>
<!--Responsive Tabs-->
<!--<script src="<?php bloginfo('template_url') ?>/js/jquery.responsive.tabs.js"></script>-->
<!--Navbar-->
<script src="<?php bloginfo('template_url') ?>/js/jquery.rd-navbar.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUg-oh-gG9iPwXttmwlJwtq7zAWU1lziw"></script>
<script src="<?php bloginfo('template_url') ?>/js/jquery.rd-google-map.js"></script>
<!--Owl Carousel-->
<script src="<?php bloginfo('template_url') ?>/js/owl.carousel.min.js"></script>

<script src="<?php bloginfo('template_url') ?>/js/wow.js"></script>
<script>
    var o = jQuery('#camera');
    if (o.length > 0) {
        if (!(isIE() && (isIE() > 9))) {
            var alertJs = document.createElement('script');
            alertJs.type = 'text/javascript';
            alertJs.src = '<?php bloginfo('template_url') ?>/js/jquery.mobile.customized.min.js';
            jQuery('body').append(alertJs);
        }

        var alertJs = document.createElement('script');
        alertJs.type = 'text/javascript';
        alertJs.src = '<?php bloginfo('template_url') ?>/js/camera.js';
        jQuery('body').append(alertJs);
    }

//    Search.js
    var o = jQuery('.search-form');
    if (o.length > 0) {
        var alertJs = document.createElement('script');
        alertJs.type = 'text/javascript';
        alertJs.src = '<?php bloginfo('template_url') ?>/js/TMSearch.js';
        jQuery('body').append(alertJs);
    }
</script>
<!--Parallax-->
<script src="<?php bloginfo('template_url') ?>/js/jquery.rd-parallax.js"></script>


<script src="<?php bloginfo('template_url') ?>/js/tm-scripts.js"></script> 

<!-- </script> -->
<script>
    $(document).ready(function () {
        $('.sf-menu .dropdown>a').append('<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>');
    });
</script>

</body>
</html>
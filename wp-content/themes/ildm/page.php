<?php
get_header();
?>
<main>
    <section class="well well4">
        <div class="container">
            <h2>
                <?php the_title(); ?>
            </h2>
            <div class="row offs2">
                <div class="col-lg-12">
                    <?php
                    if (have_posts()):
                        the_post();
                        ?>
                        <?php the_content(); ?>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
get_footer();

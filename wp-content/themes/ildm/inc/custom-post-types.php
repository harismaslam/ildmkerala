<?php

//  Hook the 'custom_post_types_init' function executes
//   after WordPress has loaded.
add_action('init', 'custom_post_types_init');

//  The function 'custom_post_types_init' creates
//  the custom post type (CPT): 'SommerGwunder', 'WinterGwunder',
//  'group deals' and 'Tips & Tricks'.
function custom_post_types_init() {
    //  Array containing the labels for the CPT 'Parallax'
    $slider_labels = array(
        'name' => _x('Slider', 'general name'),
        'singular_name' => _x('Slider', 'singular name'),
        'menu_name' => _x('Slider', 'admin menu'),
        'name_admin_bar' => _x('Slider', 'add new on admin bar'),
        'add_new' => _x('Add New', 'slider'),
        'add_new_item' => __('Add New'),
        'edit_item' => __('Edit'),
        'new_item' => __('New'),
        'all_items' => __('All'),
        'view_item' => __('View'),
        'search_items' => __('Search'),
        'not_found' => __('No items found'),
        'not_found_in_trash' => __('No items found in the Trash'),
        'parent_item_colon' => '',
    );

    $slider_args = array(
        'labels' => $slider_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slider'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail'),
    );    

    $training_labels = array(
        'name' => _x('Trainings', 'post type general name'),
        'singular_name' => _x('Training', 'post type singular name'),
        'menu_name' => _x('Trainings', 'admin menu'),
        'name_admin_bar' => _x('Training', 'add new on admin bar'),
        'add_new' => _x('Add new', 'training'),
        'add_new_item' => __('Add new'),
        'new_item' => __('New Training'),
        'edit_item' => __('Edit Training'),
        'view_item' => __('View Training'),
        'all_items' => __('All Training'),
        'search_items' => __('Search Training'),
        'parent_item_colon' => __('Parent post:'),
        'not_found' => __('No post found.'),
        'not_found_in_trash' => __('No post found in Trash.'),
    );

    $training_args = array(
        'labels' => $training_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'training'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title'),
    );
    
    $news_labels = array(
        'name' => _x('News', 'post type general name'),
        'singular_name' => _x('News', 'post type singular name'),
        'menu_name' => _x('News', 'admin menu'),
        'name_admin_bar' => _x('News', 'add new on admin bar'),
        'add_new' => _x('Add new', 'News'),
        'add_new_item' => __('Add new'),
        'new_item' => __('New'),
        'edit_item' => __('Edit'),
        'view_item' => __('View '),
        'all_items' => __('All'),
        'search_items' => __('Search'),
        'parent_item_colon' => __('Parent post:'),
        'not_found' => __('No post found.'),
        'not_found_in_trash' => __('No post found in Trash.'),
    );
    $news_args = array(
        'labels' => $news_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'news'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail'),
    );

    $course_labels = array(
        'name' => _x('Course', 'post type general name'),
        'singular_name' => _x('Course', 'post type singular name'),
        'menu_name' => _x('Course', 'admin menu'),
        'name_admin_bar' => _x('Course', 'add new on admin bar'),
        'add_new' => _x('Add new', 'course'),
        'add_new_item' => __('Add new'),
        'new_item' => __('New Course'),
        'edit_item' => __('Edit Course'),
        'view_item' => __('View Course'),
        'all_items' => __('All Courses'),
        'search_items' => __('Search Course'),
        'parent_item_colon' => __('Parent post:'),
        'not_found' => __('No post found.'),
        'not_found_in_trash' => __('No post found in Trash.'),
    );

    $course_args = array(
        'labels' => $course_labels,
        'public' => true,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'course'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title'),
    );

    register_post_type('slider', $slider_args);
    register_post_type('training', $training_args);
    register_post_type('news', $news_args);
    register_post_type('course', $course_args);
}

<?php
/**
 * For adding theme options in the backend
 */
add_action('init', 'hma_admin_init');
add_action('admin_menu', 'hma_settings_page_init');

function hma_admin_init() {
    $settings = get_option("hma_theme_settings");
    if (empty($settings)) {
        $settings = array(
        );
        add_option("hma_theme_settings", $settings, '', 'yes');
    }
}

function hma_settings_page_init() {
    $theme_data = get_theme_data(TEMPLATEPATH . '/style.css');
    $settings_page = add_theme_page($theme_data['Name'] . ' Theme Settings', $theme_data['Name'] . ' Theme Settings', 'edit_theme_options', 'theme-settings', 'hma_settings_page');
    add_action("load-{$settings_page}", 'hma_load_settings_page');
}

function hma_load_settings_page() {
    if ($_POST["hma-settings-submit"] == 'Y') {
        check_admin_referer("hma-settings-page");
        hma_save_theme_settings();
        $url_parameters = isset($_GET['tab']) ? 'updated=true&tab=' . $_GET['tab'] : 'updated=true';
        wp_redirect(admin_url('themes.php?page=theme-settings&' . $url_parameters));
        exit;
    }
}

function hma_save_theme_settings() {
    global $pagenow;
    $settings = get_option("hma_theme_settings");

    if ($pagenow == 'themes.php' && $_GET['page'] == 'theme-settings') {
        if (isset($_GET['tab']))
            $tab = $_GET['tab'];
        else
            $tab = 'general';

        switch ($tab) {
            case 'general' :
                $settings['hma_phone_number'] = $_POST['hma_phone_number'];
                $settings['hma_email_id'] = $_POST['hma_email_id'];
                break;
            case 'home' :
                $settings['hma_home_block1'] = $_POST['hma_home_block1'];
                $settings['hma_home_block1_ml'] = $_POST['hma_home_block1_ml'];
                
                $settings['hma_home_block2'] = $_POST['hma_home_block2'];
                $settings['hma_home_block2_ml'] = $_POST['hma_home_block2_ml'];
                
                $settings['hma_home_block3'] = $_POST['hma_home_block3'];
                $settings['hma_home_block3_ml'] = $_POST['hma_home_block3_ml'];
                
                $settings['hma_home_block4'] = $_POST['hma_home_block4'];
                $settings['hma_home_block4_ml'] = $_POST['hma_home_block4_ml'];
                
                $settings['hma_home_block5'] = $_POST['hma_home_block5'];
                $settings['hma_home_block5_ml'] = $_POST['hma_home_block5_ml'];
                
                $settings['hma_home_block6'] = $_POST['hma_home_block6'];
                $settings['hma_home_block6_ml'] = $_POST['hma_home_block6_ml'];
                
                $settings['hma_home_block7'] = $_POST['hma_home_block7'];
                $settings['hma_home_block7_ml'] = $_POST['hma_home_block7_ml'];
                
                $settings['hma_home_block8'] = $_POST['hma_home_block8'];
                $settings['hma_home_block8_ml'] = $_POST['hma_home_block8_ml'];
                
                $settings['hma_home_block9'] = $_POST['hma_home_block9'];
                $settings['hma_home_block9_ml'] = $_POST['hma_home_block9_ml'];
                
                $settings['hma_home_block10'] = $_POST['hma_home_block10'];
                $settings['hma_home_block10_ml'] = $_POST['hma_home_block10_ml'];
                
                $settings['hma_home_block11'] = $_POST['hma_home_block11'];
                $settings['hma_home_block11_ml'] = $_POST['hma_home_block11_ml'];
                
                $settings['hma_home_block12'] = $_POST['hma_home_block12'];
                $settings['hma_home_block12_ml'] = $_POST['hma_home_block12_ml'];
                
                $settings['hma_home_block13'] = $_POST['hma_home_block13'];
                $settings['hma_home_block13_ml'] = $_POST['hma_home_block13_ml'];
                
                $settings['hma_home_block14'] = $_POST['hma_home_block14'];
                $settings['hma_home_block14_ml'] = $_POST['hma_home_block14_ml'];
                
                $settings['hma_home_block15'] = $_POST['hma_home_block15'];
                $settings['hma_home_block15_ml'] = $_POST['hma_home_block15_ml'];
                
                $settings['hma_home_block16'] = $_POST['hma_home_block16'];
                $settings['hma_home_block16_ml'] = $_POST['hma_home_block16_ml'];
//                $settings['hma_home_team_text'] = $_POST['hma_home_team_text'];
//                $settings['hma_home_stress_text'] = $_POST['hma_home_stress_text'];
                break;
//            case 'footer' :
//                $settings['hma_copyright_text'] = $_POST['hma_copyright_text'];
//                $settings['hma_address'] = $_POST['hma_address'];
//                $settings['hma_work_time'] = $_POST['hma_work_time'];
//                $settings['hma_map'] = $_POST['hma_map'];
//                $settings['hma_read_book_link'] = $_POST['hma_read_book_link'];
//                break;
            case 'social' :
                $settings['hma_facebook'] = $_POST['hma_facebook'];
//                $settings['hma_youtube'] = $_POST['hma_youtube'];
                $settings['hma_twitter'] = $_POST['hma_twitter'];
//                $settings['hma_googleplus'] = $_POST['hma_googleplus'];
                $settings['hma_linkedin'] = $_POST['hma_linkedin'];
                break;
        }
    }

    if (!current_user_can('unfiltered_html')) {
        if ($settings['hma_copyright_text']) {
            $settings['hma_copyright_text'] = stripslashes(esc_textarea(wp_filter_post_kses($settings['hma_copyright_text'])));
        }
    }

    $updated = update_option("hma_theme_settings", $settings);
}

function hma_admin_tabs($current = 'homepage') {
//    $tabs = array('general' => 'General', 'home' => 'Home Page', 'social' => 'Social', 'footer' => 'Footer');
    $tabs = array('general' => 'General', 'home' => 'Home Page', 'social' => 'Social');
    $links = array();
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach ($tabs as $tab => $name) {
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-settings&tab=$tab'>$name</a>";
    }
    echo '</h2>';
}

function hma_settings_page() {
    global $pagenow;
    $settings = get_option("hma_theme_settings");
    $theme_data = get_theme_data(TEMPLATEPATH . '/style.css');
    ?>

    <div class="wrap">
        <h2><?php echo $theme_data['Name']; ?> Theme Settings</h2>

        <?php
        if ('true' == esc_attr($_GET['updated']))
            echo '<div class="updated" ><p>Theme Settings updated.</p></div>';

        if (isset($_GET['tab']))
            hma_admin_tabs($_GET['tab']);
        else
            hma_admin_tabs('homepage');
        ?>

        <div id="poststuff">
            <form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>">
                <?php
                wp_nonce_field("hma-settings-page");

                if ($pagenow == 'themes.php' && $_GET['page'] == 'theme-settings') {

                    if (isset($_GET['tab']))
                        $tab = $_GET['tab'];
                    else
                        $tab = 'general';

                    echo '<table class="form-table">';
                    switch ($tab) {
                        case 'general' :
                            ?>
                            <tr>
                                <th><label for="hma_phone_number">Phone Number</label></th>
                                <td>
                                    <input id="hma_phone_number" size="60" name="hma_phone_number" type="text" value="<?php echo $settings["hma_phone_number"] ?>"/><br/>
                                    <span class="description">Enter Phone Number</span>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_email_id">Email</label></th>
                                <td>
                                    <input id="hma_email_id" size="60" name="hma_email_id" type="text" value="<?php echo $settings["hma_email_id"] ?>"/><br/>
                                    <span class="description">Enter Email ID</span>
                                </td>
                            </tr>
                            <?php
                            break;
                        case 'home' :
                            ?>
                            <h2>Home Page Blocks</h2>
                            <tr>
                                <th><label for="hma_home_block1">Block 1 Id</label></th>
                                <td>
                                    <input id="hma_home_block1" size="60" name="hma_home_block1" type="text" value="<?php echo $settings["hma_home_block1"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block1_ml">Block 1 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block1_ml" size="60" name="hma_home_block1_ml" type="text" value="<?php echo $settings["hma_home_block1_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block2">Block 2 Id</label></th>
                                <td>
                                    <input id="hma_home_block2" size="60" name="hma_home_block2" type="text" value="<?php echo $settings["hma_home_block2"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block2_ml">Block 2 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block2_ml" size="60" name="hma_home_block2_ml" type="text" value="<?php echo $settings["hma_home_block2_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block3">Block 3 Id</label></th>
                                <td>
                                    <input id="hma_home_block3" size="60" name="hma_home_block3" type="text" value="<?php echo $settings["hma_home_block3"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block3_ml">Block 3 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block3_ml" size="60" name="hma_home_block3_ml" type="text" value="<?php echo $settings["hma_home_block3_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block4">Block 4 Id</label></th>
                                <td>
                                    <input id="hma_home_block4" size="60" name="hma_home_block4" type="text" value="<?php echo $settings["hma_home_block4"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block4_ml">Block 4 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block4_ml" size="60" name="hma_home_block4_ml" type="text" value="<?php echo $settings["hma_home_block4_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block5">Block 5 Id</label></th>
                                <td>
                                    <input id="hma_home_block5" size="60" name="hma_home_block5" type="text" value="<?php echo $settings["hma_home_block5"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block5_ml">Block 5 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block5_ml" size="60" name="hma_home_block5_ml" type="text" value="<?php echo $settings["hma_home_block5_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block6">Block 6 Id</label></th>
                                <td>
                                    <input id="hma_home_block6" size="60" name="hma_home_block6" type="text" value="<?php echo $settings["hma_home_block6"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block6_ml">Block 6 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block6_ml" size="60" name="hma_home_block6_ml" type="text" value="<?php echo $settings["hma_home_block6_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block7">Block 7 Id</label></th>
                                <td>
                                    <input id="hma_home_block7" size="60" name="hma_home_block7" type="text" value="<?php echo $settings["hma_home_block7"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block7_ml">Block 7 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block7_ml" size="60" name="hma_home_block7_ml" type="text" value="<?php echo $settings["hma_home_block7_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block8">Block 8 Id</label></th>
                                <td>
                                    <input id="hma_home_block8" size="60" name="hma_home_block8" type="text" value="<?php echo $settings["hma_home_block8"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block8_ml">Block 8 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block8_ml" size="60" name="hma_home_block8_ml" type="text" value="<?php echo $settings["hma_home_block8_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block9">Block 9 Id</label></th>
                                <td>
                                    <input id="hma_home_block9" size="60" name="hma_home_block9" type="text" value="<?php echo $settings["hma_home_block9"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block9_ml">Block 9 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block9_ml" size="60" name="hma_home_block9_ml" type="text" value="<?php echo $settings["hma_home_block9_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block10">Block 10 Id</label></th>
                                <td>
                                    <input id="hma_home_block10" size="60" name="hma_home_block10" type="text" value="<?php echo $settings["hma_home_block10"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block10_ml">Block 10 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block10_ml" size="60" name="hma_home_block10_ml" type="text" value="<?php echo $settings["hma_home_block10_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block11">Block 1 Id</label></th>
                                <td>
                                    <input id="hma_home_block11" size="60" name="hma_home_block11" type="text" value="<?php echo $settings["hma_home_block11"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block11_ml">Block 11 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block11_ml" size="60" name="hma_home_block11_ml" type="text" value="<?php echo $settings["hma_home_block11_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block12">Block 12 Id</label></th>
                                <td>
                                    <input id="hma_home_block12" size="60" name="hma_home_block12" type="text" value="<?php echo $settings["hma_home_block12"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block12_ml">Block 12 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block12_ml" size="60" name="hma_home_block12_ml" type="text" value="<?php echo $settings["hma_home_block12_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block13">Block 13 Id</label></th>
                                <td>
                                    <input id="hma_home_block13" size="60" name="hma_home_block13" type="text" value="<?php echo $settings["hma_home_block13"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block13_ml">Block 13 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block13_ml" size="60" name="hma_home_block13_ml" type="text" value="<?php echo $settings["hma_home_block13_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block14">Block 14 Id</label></th>
                                <td>
                                    <input id="hma_home_block14" size="60" name="hma_home_block14" type="text" value="<?php echo $settings["hma_home_block14"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block14_ml">Block 14 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block14_ml" size="60" name="hma_home_block14_ml" type="text" value="<?php echo $settings["hma_home_block14_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block15">Block 15 Id</label></th>
                                <td>
                                    <input id="hma_home_block15" size="60" name="hma_home_block15" type="text" value="<?php echo $settings["hma_home_block15"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block15_ml">Block 15 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block15_ml" size="60" name="hma_home_block15_ml" type="text" value="<?php echo $settings["hma_home_block15_ml"] ?>"/><br/>
                                </td>
                            </tr>
                            
                            <tr>
                                <th><label for="hma_home_block16">Block 16 Id</label></th>
                                <td>
                                    <input id="hma_home_block16" size="60" name="hma_home_block16" type="text" value="<?php echo $settings["hma_home_block16"] ?>"/><br/>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_home_block16_ml">Block 16 Id - Ml</label></th>
                                <td>
                                    <input id="hma_home_block16_ml" size="60" name="hma_home_block16_ml" type="text" value="<?php echo $settings["hma_home_block16_ml"] ?>"/><br/>
                                </td>
                            </tr>
                <!--                            <tr>
                            <th><label for="hma_home_stress_text">Why Live With Stress</label></th>
                            <td>
                                <textarea id="hma_home_stress_text" name="hma_home_stress_text" cols="60" rows="5"><?php echo esc_html(stripslashes($settings["hma_home_stress_text"])); ?></textarea><br/>
                                <span class="description">Enter your description</span>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="hma_home_team_text">Who we are</label></th>
                            <td>
                                <textarea id="hma_home_team_text" name="hma_home_team_text" cols="60" rows="5"><?php echo esc_html(stripslashes($settings["hma_home_team_text"])); ?></textarea><br/>
                                <span class="description">Enter your description</span>
                            </td>
                        </tr>-->
                            <?php
                            break;
                        case 'footer' :
                            ?>
                            <tr>
                                <th><label for="hma_address">Address</label></th>
                                <td>
                                    <textarea id="hma_address" name="hma_address" cols="60" rows="5"><?php echo esc_html(stripslashes($settings["hma_address"])); ?></textarea><br/>
                                    <span class="description">Enter your Address</span>
                                </td>
                            </tr>
                <!--                            <tr>
                                <th><label for="hma_work_time">Hours of Operation</label></th>
                                <td>
                                    <textarea id="hma_work_time" name="hma_work_time" cols="60" rows="5"><?php echo esc_html(stripslashes($settings["hma_work_time"])); ?></textarea><br/>
                                    <span class="description">Enter your office hours details here</span>
                                </td>
                            </tr>-->
                            <tr>
                                <th><label for="hma_map">Map</label></th>
                                <td>
                                    <textarea id="hma_map" name="hma_map" cols="60" rows="5"><?php echo $settings["hma_map"]; ?></textarea><br/>
                                    <span class="description">Enter your maps coordinates</span>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_copyright_text">Copyright text</label></th>
                                <td>
                                    <textarea id="hma_copyright_text" name="hma_copyright_text" cols="60" rows="5"><?php echo esc_html(stripslashes($settings["hma_copyright_text"])); ?></textarea><br/>
                                    <span class="description">Enter your footer copyright text</span>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="hma_read_book_link">Read book link</label></th>
                                <td>
                                    <input id="hma_read_book_link" size="60" name="hma_read_book_link" type="text" value="<?php echo $settings["hma_read_book_link"] ?>"/><br/>
                                    <span class="description">Enter your read book link</span>
                                </td>
                            </tr>
                            <?php
                            break;
                        case 'social' :
                            ?>
                            <tr>
                                <th><label for="hma_facebook">Facebook</label></th>
                                <td>
                                    <input id="hma_facebook" size="60" name="hma_facebook" type="text" value="<?php echo $settings["hma_facebook"] ?>"/><br/>
                                    <span class="description">Enter facebook link</span>
                                </td>
                            </tr>
                <!--                            <tr>
                                <th><label for="hma_youtube">Youtube</label></th>
                                <td>
                                    <input id="hma_youtube" size="60" name="hma_youtube" type="text" value="<?php echo $settings["hma_youtube"] ?>"/><br/>
                                    <span class="description">Enter youtube link</span>
                                </td>
                            </tr>-->
                            <tr>
                                <th><label for="hma_twitter">Twitter</label></th>
                                <td>
                                    <input id="hma_twitter" size="60" name="hma_twitter" type="text" value="<?php echo $settings["hma_twitter"] ?>"/><br/>
                                    <span class="description">Enter twitter link</span>
                                </td>
                            </tr>
                <!--                            <tr>
                                <th><label for="hma_googleplus">Google Plus</label></th>
                                <td>
                                    <input id="hma_googleplus" size="60" name="hma_googleplus" type="text" value="<?php echo $settings["hma_googleplus"] ?>"/><br/>
                                    <span class="description">Enter google plus link</span>
                                </td>
                            </tr>-->
                            <tr>
                                <th><label for="hma_linkedin">Linkedin</label></th>
                                <td>
                                    <input id="hma_linkedin" size="60" name="hma_linkedin" type="text" value="<?php echo $settings["hma_linkedin"] ?>"/><br/>
                                    <span class="description">Enter Linkedin link</span>
                                </td>
                            </tr>
                            <?php
                            break;
                    }
                    echo '</table>';
                }
                ?>
                <p class="submit" style="clear: both;">
                    <input type="submit" name="Submit"  class="button-primary" value="Update Settings" />
                    <input type="hidden" name="hma-settings-submit" value="Y" />
                </p>
            </form>
            <p><?php echo $theme_data['Name'] ?> theme by <a href="http://facebook.com/harismaslam">HMA</a>!</p>
        </div>

    </div>
    <?php
}
?>
<?php
/*
 * Participants list
 */

add_action('admin_menu', 'paticipant_admin_page');

function paticipant_admin_page() {
    add_menu_page('Participants', 'Participants', 'manage_options', 'participants-list', 'participants_list');
}

function participants_list() {
    global $wpdb;
    ?>
    <div class="wrap">
        <h1>Participants List</h1>
        <?php $participants = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'participants', OBJECT); ?>
        <?php if (!empty($participants)): ?>
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="Table-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Training Date</th>
                            <th>Course</th>
                            <!--<th class="nosort"></th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($participants as $val): ?>
                            <tr>
                                <td>
                                    <?php echo $val->trainee_name; ?>
                                </td>
                                <td>
                                    <?php echo date('d/m/Y', strtotime($val->training_date)); ?>
                                </td>
                                <td>
                                    <?php echo get_the_title($val->training_course_id); ?>
                                </td>
            <!--                                <td>
                                <?php echo $val->training_date; ?>
                                </td>-->
                            </tr>    
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery('#Table-list').DataTable({
                dom: 'Bfrtip',
                aoColumnDefs: [{
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }],
                aaSorting: [],
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Export'
                    }
                ]
            });
        });
    </script>
    <?php
}

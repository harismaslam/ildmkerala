<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="format-detection" content="telephone=no"/>
        <link rel="icon" href="<?php bloginfo('template_url') ?>/images/favicon.ico" type="image/x-icon">
        <title>
            <?php
            global $page, $paged;
            wp_title('|', true, 'right');
            // Add the blog name.
            bloginfo('name');
            // Add the blog description for the home/front page.
            $site_description = get_bloginfo('description', 'display');
            if ($site_description && ( is_home() || is_front_page() ))
                echo " | $site_description";
            // Add a page number if necessary:
            if ($paged >= 2 || $page >= 2)
                echo ' | ' . sprintf(__('Page %s', 'hos'), max($paged, $page));
            ?>
        </title>
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url') ?>/css/bootstrap.css" rel="stylesheet">

        <!-- Links -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/camera.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/search.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/google-map.css">

        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />


        <!--JS-->
        <script src="<?php bloginfo('template_url') ?>/js/jquery.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/rd-smoothscroll.min.js"></script>


        <!--[if lt IE 9]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
                <img src="<?php bloginfo('template_url') ?>/images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                     alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
            </a>
        </div>
        <script src="<?php bloginfo('template_url') ?>/js/html5shiv.js"></script>
        <![endif]-->
        <script src='<?php bloginfo('template_url') ?>/js/device.min.js'></script>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="page">
            <!--========================================================
                                      HEADER
            =========================================================-->
            <header>
                <div class="clearfix"></div>
                <div class="container top-sect">                    
                    <div class="pull-right" id="lang-switcher">
                        <?php dynamic_sidebar('language_widget_area'); ?>
                    </div>
                    <div class="navbar-header visible-sm visible-md visible-lg">
                        <h1 class="navbar-brand">
                            <a data-type='rd-navbar-brand'  href="<?php bloginfo('home'); ?>">
                                <img src="<?php bloginfo('template_url') ?>/images/logo.png"/>
                            </a>
                        </h1>
                    </div>
                </div>
                <div id="stuck_container" class="stuck_container">
                    <div class="container">   
                        <nav class="navbar navbar-default navbar-static-top pull-left">
                            <div class="">
                                <?php get_template_part('inc/menu'); ?>
                            </div>
                        </nav>
                    </div>
                </div>  
            </header>
